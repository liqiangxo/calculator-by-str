# 公式
## 介绍
### 字符串公式插件
### calculator-by-str功能
    输入字符串公式，得到最终计算值 请看代码注释
### born-fm功能
    用于生成公式 详细细节 请看代码注释

## 安装
    npm i calculator-by-str

## calculator-by-str使用
``` js
const { fmFunc } = require("./calculator-by-str"); // 公式计算工具
let ret = fmFunc("1+1+(Math.round(1.5)*Math.sin(-0.5))")
console.log(ret)
```
## born-fm使用
```js
const { getRandomFm, ErrorTree } = require("./born-fm"); // 生成公式
const layer = 3 // 复杂度
let [newFm, newNum] = getRandomFm(layer)
// newFm 生成的公式
// newNum 生成公式的值 这个值的算法 和 calculator-by-str的算法不同
// 用newNum 的值 和 fmFunc(newFm) 的值比较 就知道算法是否正确

```

## 支持
1. 支持函数随意增加括号，但要保持公式正确。
2. 支持检查函数是否正确，检查Math函数参数个数是否正确，检查除0问题，如果有问题直接返回NaN。
3. 支持生成字符串公式 born-fm.js
4. 支持控制难度测试test.js - layer
5. 支持测试进度条显示
6. 支持json公式测试，outMath.json
7. 支持公式写入文件 printToFile
8. 支持忽略大小写 Math.round = Math.round


```js    
// 支持的函数
const formulas = {
    "Math.abs": [Math.abs, 1],
    "Math.acos": [Math.acos, 1],
    "Math.asin": [Math.asin, 1],
    "Math.atan": [Math.atan, 1],
    "Math.atan2": [Math.atan2, 2],
    "Math.ceil": [Math.ceil, 1],
    "Math.cos": [Math.cos, 1],
    "Math.floor": [Math.floor, 1],
    "Math.log": [Math.log, 1],
    "Math.max": [Math.max, 2], // max 为了和其他语言同步 改成两个 如果需要用多个参数 可以把2改成"more"
    "Math.min": [Math.min, 2], // min 为了和其他语言同步 改成两个 如果需要用多个参数 可以把2改成"more"
    "Math.pow": [Math.pow, 2],
    "Math.round": [round, 1],
    "Math.sin": [Math.sin, 1],
    "Math.sqrt": [Math.sqrt, 1],
    "Math.tan": [Math.tan, 1],

    "Math.mod": [mod, 2], // 非Math方法

    "Math.PI": [Math.PI],
    // "Math.SQRT2": [Math.SQRT2],// 需要的话可以打开注释，测试文件记得同步打开注释
    // "Math.LN10": [Math.LN10],
    // "Math.LOG10E": [Math.LOG10E],
    // "Math.LOG2E": [Math.LOG2E],
    // "Math.SQRT1_2": [Math.SQRT1_2],
    // "Math.SQRT2": [Math.SQRT2],
}

// 支持的计算符号
1. +
2. -
3. *
4. /
5. **
```


## 注意
1. 目前没有使用JavaScript下面的`math.round` 可能与其他的语言计算方式不同，详见[JavaScript Math.round()四舍五入误差问题](https://www.jianshu.com/p/06c0dde3ba3a)。所有为了各种语言都是一样的计算结果，建议全部使用`math.floor(x+0.5)`的方式
2. 取模（求余数）`mod`，`math`下面没有实现，JavaScript里面可以使用`%`符号求余数，但是`Go`语言里面的`%`取模不支持小数，`go`的`math.mod`支持，所以程序里面，没有使用`%`，而是包装在`math.mod`里面了，字符串里面可以使用`Math.mod(0.5)`。
