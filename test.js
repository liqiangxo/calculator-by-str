'use strict';
var fs = require("fs");

// let outMath = []
const outMathDt = fs.readFileSync("./outMath.json");
let outMath = JSON.parse(outMathDt)
outMath = null //注释了可以测试json

/**
 * 测试用例
 */

const { getRandomFm, ErrorTree } = require("./born-fm"); // 生成公式
const { fmFunc } = require("./calculator-by-str"); // 公式计算工具
var slog = require('single-line-log').stdout; // 执行进度条

function printToFile(str, layer) {
    fs.writeFile("fmStr" + layer + ".log", str, { flag: "a" }, function (err) {
        if (!err) {
            console.log("写入成功！");
        }
    });
}

/**
 * 一个进度条
 * @param {*} cnt 
 */
function getBar(index, cnt) {
    let str = "["
    for (var i = 0; i < 100; i++) {
        if (Math.ceil((index + 1) / cnt * 100) > i) {
            str += "*"
        } else {
            str += " "
        }
    }
    str += "] " + (index + 1) + "/" + cnt + " " + Math.ceil((index + 1) / cnt * 100) + "%\n"
    slog(str);
}


function myLog(...args) {
    console.log(...args)
}

/**
 * 自动生成公式，自动完成测试
 * @param {*} cnt  公式数量
 * @param {*} layer  公式复杂度
 */
function test(cnt, layer, showTestDataCnt) {
    // test 
    let error = 0
    let fileData = ""
    // let fileData = []
    for (let index = 0; index < cnt; index++) {
        getBar(index, cnt)
        // myLog(curFm)
        let [newFm, newNum] = outMath ? outMath[index] : getRandomFm(layer)
        // myLog(newFm, newNum)
        // let ret = fmFunc(newFm)
        let ret = fmFunc(newFm)
        newNum = Number(newNum)
        // myLog({ ret, newFm, newNum })
        fileData += newFm + " = " + newNum + "\n"
        // fileData.push([newFm, newNum])

        try {
            let iserr = ret.toFixed(6) != newNum.toFixed(6)
            if (iserr) {
                myLog({ iserr, newFm, ret, newNum })
                ErrorTree()
                error += 1
                // break
            } else if (index < showTestDataCnt) {
                myLog("显示" + (index + 1) + "/" + + showTestDataCnt + "个测试数据：", [iserr, newFm, ret, newNum])
            }
        } catch (e) {
            error += 1
            console.log("ret", ret, e);
        }
    }
    if (!outMath) {
        printToFile(fileData, layer)
        // printToFile(JSON.stringify(fileData,null,"  "), layer)
    }
    return error
}


myLog("开始测试")
let t = new Date()
const cnt = !!outMath ? outMath.length : 10000
const layer = 5
const showTestDataCnt = 0
const error = test(cnt, Math.max(1, layer), showTestDataCnt)
myLog("测试结果：\n测试数量cnt：" + cnt + "\n每个公式难度等级：" + layer + "\n正确率:" + ((cnt - error) / cnt * 100) + "%\n测试时间:" + (new Date() - t) + "毫秒\n平均耗时:" + (new Date() - t) / cnt + "毫秒")


// let fmStr = "3*-6**2"
// // fmStr = "Math.max(-57,30,68,-94,67)"
// // fmStr = "Math.floor(68)"
// myLog(fmStr, "fmStr")
// let ret = fmFunc(fmStr)
// myLog(ret, new Date() - t);

 // 还可以优化的点
 // 1. 使用栈的方式解析括号
 // 2. 优化如果0*(fmStr)  这个时候 fmStr 是可以不计算的